package com.osf.sp.service;

import java.util.List;

import com.osf.sp.vo.DeptVO;

public interface DeptService {
	public List<DeptVO> selectDeptList();
}
