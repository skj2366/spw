package com.osf.sp.service;

import java.util.List;

import com.osf.sp.vo.EmpVO;

public interface EmpService {

	public List<EmpVO> selectEmpList();
}
