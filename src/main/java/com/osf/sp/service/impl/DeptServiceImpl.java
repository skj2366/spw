package com.osf.sp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.osf.sp.dao.DeptDAO;
import com.osf.sp.service.DeptService;
import com.osf.sp.vo.DeptVO;

@Service
public class DeptServiceImpl implements DeptService {

	@Autowired
	private DeptDAO ddao;
	@Override
	public List<DeptVO> selectDeptList() {
		return ddao.selectDeptList();
	}

}
