package com.osf.sp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.osf.sp.dao.EmpDAO;
import com.osf.sp.service.EmpService;
import com.osf.sp.vo.EmpVO;

@Service
public class EmpServiceImpl implements EmpService {

	@Autowired
	private EmpDAO edao;
	@Override
	public List<EmpVO> selectEmpList() {
		return edao.selectEmpList();
	}

}
