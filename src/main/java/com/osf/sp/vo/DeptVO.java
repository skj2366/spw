package com.osf.sp.vo;

import lombok.Data;

@Data
public class DeptVO {

	private Integer deptNo;
	private String dName;
	private String loc;
}
