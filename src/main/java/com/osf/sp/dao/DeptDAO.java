package com.osf.sp.dao;

import java.util.List;

import com.osf.sp.vo.DeptVO;

public interface DeptDAO {

	public List<DeptVO> selectDeptList();
}
