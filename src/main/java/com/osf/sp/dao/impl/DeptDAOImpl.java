package com.osf.sp.dao.impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.osf.sp.dao.DeptDAO;
import com.osf.sp.vo.DeptVO;

@Repository
public class DeptDAOImpl implements DeptDAO {

	@Autowired
	private SqlSession ss;
	
	@Override
	public List<DeptVO> selectDeptList() {	
		return ss.selectList("dept.selectDeptList");
	}

}
