package com.osf.sp.dao.impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.osf.sp.dao.EmpDAO;
import com.osf.sp.vo.EmpVO;

@Repository
public class EmpDAOImpl implements EmpDAO {

	@Autowired
	private SqlSession ss;
	
	@Override
	public List<EmpVO> selectEmpList() {
		return ss.selectList("emp.selectEmpList");
	}

}
