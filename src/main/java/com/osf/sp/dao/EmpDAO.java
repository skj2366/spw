package com.osf.sp.dao;

import java.util.List;

import com.osf.sp.vo.EmpVO;

public interface EmpDAO {

	public List<EmpVO> selectEmpList();
}
