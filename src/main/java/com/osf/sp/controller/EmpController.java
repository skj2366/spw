package com.osf.sp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.osf.sp.service.EmpService;

@Controller
public class EmpController {
	
	@Autowired
	private EmpService es;
	
	@RequestMapping(value="/emps", method=RequestMethod.GET)
	public String getEmpList(Model m) {
		m.addAttribute("emplist",es.selectEmpList());
		return "emp/list";
	}
}
