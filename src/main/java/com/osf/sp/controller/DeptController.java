package com.osf.sp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.osf.sp.service.DeptService;

@Controller
public class DeptController {

	@Autowired
	private DeptService ds;
	
	@RequestMapping(value="/depts", method=RequestMethod.GET)
	public String getDeptList(Model m) {
		m.addAttribute("deptlist",ds.selectDeptList());
		return "dept/list";
	}
}
